package com.Z;


import java.awt.event.ActionListener;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.Scanner;


public class ZClock_App implements ActionListener
{


    // Application components
    private JFrame app_main_frame = new JFrame("Z-Clock");
    private JTextField searchCountry_tfield;
    private JTextField userCountry_tfield;
    private JButton search_Button;
    private JButton userCountry_jbn;
    private JButton userCountry_clearLoc;
    private JLabel timestampResult_jlabel;
    private JLabel whichTimeFormat_jlab;
    private JLabel userCountry_jlab;
    private JRadioButton jrb12Format;
    private JRadioButton jrb24Format;
    private JSeparator vert_jsr;
    private Clock mainClock = new Clock(app_main_frame); // Send reference to the main JFrame


    private ZClock_App()
    {
        /* Grab user country and UTC if possible */
        try {
            File tempFile = new File("./tempFile.txt");
            Scanner readFile = new Scanner(tempFile);
            int counter = 0;

            while(readFile.hasNextLine())
            {
                switch(counter)
                {
                    case 0:
                        mainClock.setUserCountry(readFile.nextLine());
                        counter++;
                        break;
                    case 1:
                        mainClock.setUserUTC(Integer.parseInt(readFile.nextLine()));
                        counter++;
                        break;
                }
            }
        }
        catch (FileNotFoundException e)
        {
            System.out.println("File Not Found: At the beginning of ZClock_App Constructor, try-catch block test");
        }



        /* Initialize JFrame Properties */
        app_main_frame.setLayout(null);
        app_main_frame.setSize(700, 220);
        app_main_frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        app_main_frame.setLocation(dim.width/2-app_main_frame.getSize().width/2, dim.height/2-app_main_frame.getSize().height/2); // Set beginning JFrame location
        app_main_frame.setIconImage(Toolkit.getDefaultToolkit().getImage("AppIcon.png")); // Set app icon image
        app_main_frame.setContentPane(new JLabel(new ImageIcon("./MiddleDayGrass.gif"))); // Set the application background
        app_main_frame.setResizable(false); // Prevent the GUI from looking adjusted improperly

        /* Before JFrame closes, do this... */
        app_main_frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) // While the frame is closing, do this...
            {
                try {
                    PrintWriter write = new PrintWriter("tempFile.txt"); // Make this file
                    write.println(mainClock.getUserCountry());
                    write.println(mainClock.getUserUTC());
                    write.close();
                }
                catch (FileNotFoundException ea)
                {
                    // This will never be an issues since the above process is supposed to create the file every time
                }
            }
        });

        /* Set the look and feel */
        try {
            UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel"); // try the nimbus look
        }
        catch (Exception e)
        {
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()); // Just go to the default OS look and feel instead
            }
            catch(Exception ea)
            {
                System.out.println("General Exception: Issue with Look and Feel try-catch statements");
            }

        }




        /* Initialize all components and properties */

        // JTextField
        searchCountry_tfield = new JTextField("Enter a country", 20);
        userCountry_tfield = new JTextField("Enter your country", 20);
        searchCountry_tfield.setForeground(Color.gray);

        // Check if UTC is already known
        if(mainClock.getUserUTC() > 1400) {
            searchCountry_tfield.setEnabled(false); // Enabled once use Loc is known
        }
        else {
            userCountry_tfield.setText(mainClock.getUserCountry());
            userCountry_tfield.setEnabled(false);
            userCountry_tfield.setForeground(Color.gray);
        }



        // JButton
        search_Button = new JButton("SEARCH");
        userCountry_clearLoc = new JButton("CLEAR");
        userCountry_jbn = new JButton("ENTER");
        if(mainClock.getUserUTC() > 1400) {
            search_Button.setEnabled(false); // Enabled once user location is known
            userCountry_clearLoc.setEnabled(false);
        }
        else {
            userCountry_jbn.setEnabled(false);
            userCountry_clearLoc.setEnabled(true);
        }

        // JLabel
        timestampResult_jlabel = new JLabel("Results Here...", SwingConstants.CENTER);
        timestampResult_jlabel.setFont(new Font("Helvetica", Font.BOLD, 14));
        whichTimeFormat_jlab = new JLabel("Which Time Format?");
        userCountry_jlab = new JLabel("What country do you live in?");

        // JRadioButtons
        jrb12Format = new JRadioButton("12 Hour");
        jrb24Format = new JRadioButton("24 Hour");
        ButtonGroup bg = new ButtonGroup();
        bg.add(jrb12Format);
        bg.add(jrb24Format);
        // Set JRadioButtons options
        jrb12Format.setSelected(true);
        mainClock.setTimeFormat(12); // 12 hour format will be default

        // JSeperator
       vert_jsr = new JSeparator(SwingConstants.VERTICAL);
       vert_jsr.setForeground(Color.lightGray);


        // Set bounds for components
        searchCountry_tfield.setBounds(10, 10, 250, 25);
        search_Button.setBounds(270, 10, 100, 20);
        timestampResult_jlabel.setBounds(10, 40, 361, 125);
        whichTimeFormat_jlab.setBounds(425, 10, 150, 20);
        jrb12Format.setBounds(425, 35, 150, 20);
        jrb24Format.setBounds(425, 55, 150, 20);
        userCountry_jlab.setBounds(425, 85, 160, 25);
        userCountry_tfield.setBounds(425, 110, 250, 25);
        userCountry_jbn.setBounds(500, 140, 100, 25);
        vert_jsr.setBounds(400, 0, 10, 200);
        userCountry_clearLoc.setBounds(593, 90, 80, 15);

        // Set Borders
        timestampResult_jlabel.setBorder(BorderFactory.createRaisedBevelBorder());

        // Set actioncommands
        search_Button.setActionCommand("search_country_button");
        searchCountry_tfield.setActionCommand("search_country_tfield");
        jrb12Format.setActionCommand("jrb12");
        jrb24Format.setActionCommand("jrb24");
        userCountry_tfield.setActionCommand("userCountry_tfield");
        userCountry_jbn.setActionCommand("userCountry_jbn");
        userCountry_clearLoc.setActionCommand("userCountry_clearLoc");

        // Add actionlisteners to components
        searchCountry_tfield.addActionListener(this);
        search_Button.addActionListener(this);
        jrb12Format.addActionListener(this);
        jrb24Format.addActionListener(this);
        userCountry_tfield.addActionListener(this);
        userCountry_jbn.addActionListener(this);
        userCountry_clearLoc.addActionListener(this);



        // Add Components to JFrame
        app_main_frame.add(searchCountry_tfield);
        app_main_frame.add(search_Button);
        app_main_frame.add(timestampResult_jlabel);
        app_main_frame.add(whichTimeFormat_jlab);
        app_main_frame.add(jrb12Format);
        app_main_frame.add(jrb24Format);
        app_main_frame.add(userCountry_jlab);
        app_main_frame.add(userCountry_tfield);
        app_main_frame.add(userCountry_jbn);
        app_main_frame.add(vert_jsr);
        app_main_frame.add(userCountry_clearLoc);


        // Add FocusListeners
        // Ensure that the user can not interact with this hint text
        searchCountry_tfield.addFocusListener(new FocusListener() {
                @Override
                public void focusGained(FocusEvent e) {
                    if (searchCountry_tfield.getText().equals("Enter a country"))
                    {
                        searchCountry_tfield.setText("");
                    }
                    searchCountry_tfield.setForeground(Color.black);
                }

                @Override
                public void focusLost(FocusEvent e) {
                    if (searchCountry_tfield.getText().equals(""))
                    {
                        searchCountry_tfield.setText("Enter a country");
                        searchCountry_tfield.setForeground(Color.gray);
                    }
                }
            });
        userCountry_tfield.addFocusListener(new FocusListener() {
                @Override
                public void focusGained(FocusEvent e) {
                    if (userCountry_tfield.getText().equals("Enter your country"))
                    {
                        userCountry_tfield.setText("");
                    }
                    userCountry_tfield.setForeground(Color.black);
                }

                @Override
                public void focusLost(FocusEvent e) {
                    if (userCountry_tfield.getText().equals(""))
                    {
                        userCountry_tfield.setText("Enter your country");
                        userCountry_tfield.setForeground(Color.gray);
                    }
                }
            });



        app_main_frame.setVisible(true); // Set main_frame visible
        userCountry_tfield.requestFocusInWindow(); // Start off with this JTextfield
    }

    // Action performed thread
    @Override
    public void actionPerformed(ActionEvent e)
    {
        /* Change time format buttons */
        switch(e.getActionCommand())
        {
            case "jrb12":
                mainClock.setTimeFormat(12);
                if((!timestampResult_jlabel.getText().equals("")) && (!mainClock.getSearchedCountry().equals("")))
                {
                    mainClock.setSearchedStaPro_alreadyClicked(true);
                    searchCountry_tfield.setText(mainClock.getSearchedCountry());
                    search_Button.doClick();
                    searchCountry_tfield.setText("Enter a country"); // Keep the field looking like default while this process happens
                }
                mainClock.setSearchedStaPro_alreadyClicked(false);
                break;
            case "jrb24":
                mainClock.setTimeFormat(24);
                if((!timestampResult_jlabel.getText().equals("")) && (!mainClock.getSearchedCountry().equals("")))
                {
                    mainClock.setSearchedStaPro_alreadyClicked(true);
                    searchCountry_tfield.setText(mainClock.getSearchedCountry());
                    search_Button.doClick();
                    searchCountry_tfield.setText("Enter a country");  // Keep the field looking like default while this process happens
                }
                mainClock.setSearchedStaPro_alreadyClicked(false);
                break;

        }

        /* Search a country is pushed */
        if(e.getActionCommand().equals("search_country_button") || e.getActionCommand().equals("search_country_tfield"))
        {
            String previousSearchedCountry = mainClock.getSearchedCountry();
            int previousSearchedCountryUTC = mainClock.getSearchedCountry_UTC();
            if(searchCountry_tfield.getText().equals("") || searchCountry_tfield.getText().equals("Enter a country"))
            {
                JOptionPane.showMessageDialog(app_main_frame, "You must enter something!"); // Make sure user entered something
            }
            else
                {
                    mainClock.setSearchedCountry(searchCountry_tfield.getText());
                    mainClock.setSearchedCountry_UTC(mainClock.findUTC(mainClock.getSearchedCountry())); // Find UTC of searched country
                    if (mainClock.getSearchedCountry_UTC() > 1400) {
                        JOptionPane.showMessageDialog(app_main_frame, "Could not find location, please try again.");
                        mainClock.setSearchedCountry(previousSearchedCountry);
                        mainClock.setSearchedCountry_UTC(previousSearchedCountryUTC);
                    }
                    else
                        {
                        if(mainClock.getTimeFormat() == 12) // Display based off of time format
                        {
                            if(!mainClock.getSearchedStaPro().equals("")) // Check if state is involved
                            {
                                timestampResult_jlabel.setText(mainClock.conAndDisplay12() + " in " + mainClock.getSearchedStaPro() +
                                ", " + mainClock.getSearchedCountry());
                            }
                            else
                            {
                                timestampResult_jlabel.setText(mainClock.conAndDisplay12() + " in " + mainClock.getSearchedCountry());
                            }
                        }
                        else
                        {
                            if(!mainClock.getSearchedStaPro().equals("")) // Check if a state is involved
                            {
                                timestampResult_jlabel.setText(mainClock.conAndDisplay24() + " in " + mainClock.getSearchedStaPro() +
                                        ", " + mainClock.getSearchedCountry());
                            }
                            else
                            {
                                timestampResult_jlabel.setText(mainClock.conAndDisplay24() + " in " + mainClock.getSearchedCountry());
                            }
                        }
                    }
                    if(!mainClock.getSearchedStaPro_alreadyClicked())
                    searchCountry_tfield.setText(""); // Empty out search TextField

                }

        } // End of event test


        /* User country components pushed */
        else if(e.getActionCommand().equals("userCountry_tfield") || e.getActionCommand().equals("userCountry_jbn"))
        {
            //String test = mainClock.askForState(true);
            if(userCountry_tfield.getText().equals("")) // Make sure JField is not empty
            {
                JOptionPane.showMessageDialog(app_main_frame, "Please enter your country");
            }
            else
            {
                mainClock.setUserCountry(userCountry_tfield.getText()); // Save the name of the user's country
                mainClock.setUserUTC(mainClock.findUTC(userCountry_tfield.getText())); // Find the UTC
                if(mainClock.getUserUTC() > 1400) // Exception if the UTC was not found
                {
                    JOptionPane.showMessageDialog(app_main_frame, "Could not find location, please try again.");
                    mainClock.setUserCountry("");
                }
                else // UTC was found and user location is set
                {
                    mainClock.setUserCountryIsSet(true);
                    userCountry_tfield.setText(userCountry_tfield.getText().toUpperCase());
                    userCountry_tfield.setEnabled(false);
                    userCountry_jbn.setEnabled(false);
                    userCountry_clearLoc.setEnabled(true);
                    search_Button.setEnabled(true);
                    searchCountry_tfield.setEnabled(true);
                }
            }
        }

        /* Clear user country button pushed */
        else if(e.getActionCommand().equals("userCountry_clearLoc")) // Reset user's location
        {
            // Reset mainClock object fields
            mainClock.setUserCountry("");
            mainClock.setUserUTC(1500);
            mainClock.setSearchedStaPro("");
            mainClock.setSearchedCountry("");
            mainClock.setSearchedCountry_UTC(1500);
            mainClock.setUserCountryIsSet(false);

            // Reset GUI properties
            userCountry_jbn.setEnabled(true);
            userCountry_tfield.setEnabled(true);
            userCountry_clearLoc.setEnabled(false);
            userCountry_tfield.setText("");
            searchCountry_tfield.setEnabled(false);
            search_Button.setEnabled(false);
        }

    }

    public static void main(String[] args) {

       SwingUtilities.invokeLater(new Runnable() {
           @Override
           public void run() {
               new ZClock_App();
           }

       });
    }
}


