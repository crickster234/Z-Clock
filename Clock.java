package com.Z;

import com.sun.deploy.util.ArrayUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.Serializable;
import java.util.*;
import java.util.List;
import java.util.stream.Stream;


/*  Array Index to UTC Correlation
	[0] = UTC-1200
	[1] = UTC-1100
	[2] = UTC-1000
	[3] = UTC-930
	[4] = UTC-900
	[5] = UTC-800
	[6] = UTC-700
	[7] = UTC-600
	[8] = UTC-500
	[9] = UTC-400
	[10] = UTC-330
	[11] = UTC-300
	[12] = UTC-200
	[13] = UTC-100
	[14] = UTC0
	[15] = UTC100
	[16] = UTC200
	[17] = UTC300
	[18] = UTC330
	[19] = UTC400
	[20] = UTC430
	[21] = UTC500
	[22] = UTC530
	[23] = UTC545
	[24] = UTC600
	[25] = UTC630
	[26] = UTC700
	[27] = UTC800
	[28] = UTC830
	[29] = UTC845
	[30] = UTC900
	[31] = UTC930
	[32] = UTC1000
	[33] = UTC1030
	[34] = UTC1100
	[35] = UTC1200
	[36] = UTC1245
	[37] = UTC1300
	[38] = UTC1400
	*/



public class Clock
{
    // No getter or setters
    private String UTCLocations[][];
    private String moreThanOneUTC[];
    private JFrame ZClock_JFrame;

    // Has Getter and Setter Functions
    private int timeFormat; // 12 will be set as default in ZClock_App constructor
    private int userUTC;
    private String searchedCountry;
    private int searchedCountry_UTC;
    private String userCountry;
    private String searchedStaPro;
    private boolean userCounSet;
    private boolean searchedStaPro_alreadyClicked;


    public Clock(JFrame ZClockFrame)
    {
        searchedStaPro_alreadyClicked = false;
        userCounSet = false;
        searchedStaPro = "";
        userCountry = "";
        searchedCountry = "";
        userUTC = 1500;
        searchedCountry_UTC = 1500;
        ZClock_JFrame = ZClockFrame; // Load copy of JFrame
        UTCLocations = loadUTC();
        moreThanOneUTC = loadTxtFile_1Dim("./CountriesMoreThanOneUTC/List.txt");
    }

    // Set member functions

    public void setTimeFormat(int timeFormat)
    {
        this.timeFormat = timeFormat;
    }
    public void setSearchedCountry(String searchedCountry)
    {
        searchedCountry = searchedCountry.toUpperCase();
        this.searchedCountry = searchedCountry;
    }
    public void setSearchedCountry_UTC(int searchedCountry_UTC)
    {
        this.searchedCountry_UTC = searchedCountry_UTC;
    }
    public void setUserUTC(int userUTC)
    {
        this.userUTC = userUTC;
    }
    public void setUserCountry(String userCountry)
    {
        userCountry = userCountry.toUpperCase();
        this.userCountry = userCountry;
    }
    public void setUserCountryIsSet(boolean userCounSet) {this.userCounSet = userCounSet;}
    public void setSearchedStaPro_alreadyClicked(boolean isSet) {searchedStaPro_alreadyClicked = isSet;}
    public void setSearchedStaPro(String searchedStaPro) {searchedStaPro = searchedStaPro.toUpperCase(); this.searchedStaPro = searchedStaPro;}

    // Get member functions
    public int getTimeFormat() {
        return timeFormat;
    }
    public int getUserUTC() {
        return userUTC;
    }
    public String getSearchedCountry() {
        return searchedCountry;
    }
    public int getSearchedCountry_UTC() {
        return searchedCountry_UTC;
    }
    public String getUserCountry(){return userCountry;}
    public String getSearchedStaPro() {return searchedStaPro;}
    public boolean getUserCounSet() {return userCounSet;}
    public boolean getSearchedStaPro_alreadyClicked() {return searchedStaPro_alreadyClicked;}


    /*      ***UTILITY FUNCTIONS***     */
    private Calendar getSysTime()
    {
        return Calendar.getInstance();
    }
    private boolean isDST_bool_NorthHem(int minimumDate, int maximumDate)
    {
        Calendar Date = getSysTime();
        int year = Date.get(Calendar.YEAR);
        int month = Date.get(Calendar.MONTH);
        int day = Date.get(Calendar.DAY_OF_MONTH);
        boolean leap = false;
        int totalOfDate = 0;
        int theYear[] = {31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

        if(year % 4 == 0)
        {
            theYear[1] = 29;
            leap = true;
        }
        else
        {
            theYear[1] = 28;
        }

        for(int i = 0; i < (month-1); ++i)
        {
            totalOfDate += theYear[i];
        }
        totalOfDate += day;

        if(leap)
        {
           minimumDate += 1;
           maximumDate += 1;
        }


        if((totalOfDate > minimumDate) && (totalOfDate < maximumDate))
        {
            return true;
        }
        else {
            return false;
        }

    }



    /*   ***Load in UTC time zones Functions***    */
    private String[][] loadUTC()
    {
        // Declare local variables
        String UTC[][] = new String[39][100];
        int indexCounter = 0;

        for(int numberOfUTCZones = -1200; numberOfUTCZones <=1400;)
        {
                try
                {
                    File filePath = new File("./time_zones/UTC" + Integer.toString(numberOfUTCZones) + ".txt");

                    String tempLine = "";

                    Scanner currentFile = new Scanner(filePath);

                    int fileIndex = 0;

                    while(currentFile.hasNextLine())
                    {
                        tempLine = currentFile.nextLine();
                        UTC[indexCounter][fileIndex] = tempLine;
                        fileIndex++;
                    }

                    currentFile.close();

                    indexCounter++;
                }
                catch (Exception e)
                {
                    // Nothing here because some files won't open cause they don't exist
                }

                numberOfUTCZones += 25;
        }
        return UTC;
    }
    private String[] loadTxtFile_1Dim(String fileName)
    {
        String list[] = new String[19];
        List<String> temps = new ArrayList<String>();

        try {
            File filePath = new File(fileName);

            String tempLine = "";

            Scanner currentFile = new Scanner(filePath);


            while (currentFile.hasNextLine())
            {
                tempLine = currentFile.next();
                temps.add(tempLine);
            }

            currentFile.close();
        }
        catch (Exception ex)
        {
            System.out.println("morethanoneUTC didn't load\n");
        }

        list = temps.toArray(new String[19]);

        return list;
    }
    private String[] loadTxtFile_2Dim(String mainDir, String subDir, String fileName)
    {
        String list[] = new String[19];
        List<String> temps = new ArrayList<String>();

        try {
            File filePath = new File(mainDir, "/" + subDir + "/" + fileName);

            String tempLine = "";

            Scanner currentFile = new Scanner(filePath);


            while (currentFile.hasNextLine())
            {
                tempLine = currentFile.nextLine();
               temps.add(tempLine);
            }

            currentFile.close();
        }
        catch (Exception ex)
        {
            System.out.println("states didn't load\n");
        }

        list = temps.toArray(new String[19]);

        return list;
    }

    /*    ***FINDING UTC FUNCTIONS***    */
    public int findUTC(String userCountry)
    {
        // Declare local variables
        int foundIndex = -1;
        int resultUTC = -1;
        boolean isDST = false;
        int minimumDate = 0;
        int maximumDate = 0;
        boolean breakFromLoops = false;

        // Make everything uppercase
        userCountry = userCountry.toUpperCase();

        // Try to check country in regular UTC Arrays
        for(int i = 0; i < UTCLocations.length; i++)
        {

            for(int j = 0; j < UTCLocations[i].length; j++)
            {
                if(UTCLocations[i][j] != null) // Prevents null being compared with a String value and throwing exception
                {
                    if (UTCLocations[i][j].equals(userCountry)) {
                        foundIndex = i;
                        breakFromLoops = true;
                        searchedStaPro = "";
                        break;
                    }
                }
            }


            if(breakFromLoops) // Breaks again if the index was found
            {

                break;
            }

        }
        // Check if country is in more than one UTC
        for(String coun : moreThanOneUTC)
        {
            if(coun.equals(userCountry))
            {
                String firstDir = "./CountriesMoreThanOneUTC";
                if(userCountry.equals("UNITED STATES") || userCountry.equals("USA") || userCountry.equals("US") ||
                        userCountry.equals("AMERICA"))
                {
                    String subDir = "UNITED_STATES";
                    foundIndex = unitedStates(firstDir, subDir);
                }
            }
        }


        // Check if user entered a continent
        if(userCountry.equals("ANTARCTICA") || userCountry.equals("NORTH AMERICA") || userCountry.equals("AFRICA")
                || userCountry.equals("ASIA") || userCountry.equals("EUROPE") || userCountry.equals("SOUTH AMERICA"))
        {
            foundIndex = -2;
            JOptionPane.showMessageDialog(ZClock_JFrame, "That is a Continent...");
        }



        // Checks for countries with Daylight Savings
        if(userCountry.equals("UNITED KINGDOM") || userCountry.equals("UK") || userCountry.equals("ENGLAND")
                || userCountry.equals("LONDON"))
        {
            // Code for UK DST
        }



        /*             FINAL ANALYSIS OF UTC CODE HERE                  */
        if(foundIndex > -1)
        {
            int result = indexToUTC(foundIndex);
            return result;
        }
        else
        {
            return 1500;
        }

    }
    private int indexToUTC(int index)
    {
        int UTC = -1;
        switch (index)
        {
            case 0:
                UTC = -1200;
                break;
            case 1:
                UTC = -1100;
                break;
            case 2:
                UTC = -1000;
                break;
            case 3:
                UTC = -930;
                break;
            case 4:
                UTC = -900;
                break;
            case 5:
                UTC = -800;
                break;
            case 6:
                UTC = -700;
                break;
            case 7:
                UTC = -600;
                break;
            case 8:
                UTC = -500;
                break;
            case 9:
                UTC = -400;
                break;
            case 10:
                UTC = -330;
                break;
            case 11:
                UTC = -300;
                break;
            case 12:
                UTC = -200;
                break;
            case 13:
                UTC = -100;
                break;
            case 14:
                UTC = 0;
                break;
            case 15:
                UTC = 100;
                break;
            case 16:
                UTC = 200;
                break;
            case 17:
                UTC = 300;
                break;
            case 18:
                UTC = 330;
                break;
            case 19:
                UTC = 400;
                break;
            case 20:
                UTC = 430;
                break;
            case 21:
                UTC = 500;
                break;
            case 22:
                UTC = 530;
                break;
            case 23:
                UTC = 545;
                break;
            case 24:
                UTC = 600;
                break;
            case 25:
                UTC = 630;
                break;
            case 26:
                UTC = 700;
                break;
            case 27:
                UTC = 800;
                break;
            case 28:
                UTC = 830;
                break;
            case 29:
                UTC = 845;
                break;
            case 30:
                UTC = 900;
                break;
            case 31:
                UTC = 930;
                break;
            case 32:
                UTC = 1000;
                break;
            case 33:
                UTC = 1030;
                break;
            case 34:
                UTC = 1100;
                break;
            case 35:
                UTC = 1200;
                break;
            case 36:
                UTC = 1245;
                break;
            case 37:
                UTC = 1300;
                break;
            case 38:
                UTC = 1400;
                break;
            default:
                break;
        }
        return UTC;
    }


    /*    ***MULTIPLE UTC FUNCTIONS***   */
    private String lockedComboBox_Dialog(String question, JComboBox options)
    {
        String result;
        boolean returnNow;

        // Make dialog and set properties
        JDialog Dialog =  new JDialog(ZClock_JFrame, "Info Needed", true);
        Dialog.setSize(200, 135);
        Dialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        Dialog.setLayout(new FlowLayout());
        Dialog.setLocationRelativeTo(ZClock_JFrame);

        // Initialize dialog components
        JLabel Question = new JLabel("What Province?");
        Question.setText(question);
        JButton ok = new JButton("Ok");

        // Add components to dialog
        Dialog.add(Question);
        Dialog.add(options);
        Dialog.add(ok);

        options.addKeyListener(new KeyAdapter() // Add listener for if enter key was pressed in combobox
        {
            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER)
                {
                    Dialog.setVisible(false);
                }
            }
        });

        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Dialog.setVisible(false);
            }
        });

        // Set it visible
        Dialog.setVisible(true);

        // Try to return the user's choice or catch any errors
        try {
            return options.getSelectedItem().toString();
        }
        catch (NullPointerException e)
        {
            System.out.println("NullPointerException");
        }

        return "";
    }
    private int unitedStates(String firstDir, String subDir) {
        int UTC = -1;
        String state = "";
        boolean isDST_US;

        // Declare arrays for each type of time zone
        String eastern_Time[];
        String central_Time[];
        String mountain_Time[];
        String pacific_Time[];
        String options_arr[];


        // Load in states
        eastern_Time = loadTxtFile_2Dim(firstDir, subDir, "EASTERN_TIME.txt");
        central_Time = loadTxtFile_2Dim(firstDir, subDir, "CENTRAL_TIME.txt");
        mountain_Time = loadTxtFile_2Dim(firstDir, subDir, "MOUNTAIN_TIME.txt");
        pacific_Time = loadTxtFile_2Dim(firstDir, subDir, "PACIFIC_TIME.txt");
        options_arr = loadTxtFile_2Dim(firstDir, subDir, "OPTIONS.txt"); Arrays.sort(options_arr);


        // Ask for state
        if((!searchedStaPro_alreadyClicked)) // Check if this is a new entry or same state
        {
            @SuppressWarnings("unchecked") // Unchecked warning on next line, ignore it
            JComboBox options = new JComboBox(options_arr);
            state = lockedComboBox_Dialog("What State?", options);
        }
        else
        {
            state = searchedStaPro;
        }


        // Get DST
        int minimumDate = 70;
        int maximumDate = 308;
        isDST_US = isDST_bool_NorthHem(minimumDate, maximumDate);

        if(Arrays.asList(eastern_Time).contains(state))
        {
            if(isDST_US)
            {
                UTC = 9;
            }
            else
            {
                UTC = 8;
            }
        }
        else if(Arrays.asList(central_Time).contains(state))
        {
            if(isDST_US)
            {
                UTC = 8;
            }
            else
            {
                UTC = 7;
            }
        }
        else if(Arrays.asList(mountain_Time).contains(state))
        {
            if(isDST_US)
            {
                UTC = 7;
            }
            else
            {
                UTC = 6;
            }
        }
        else if(state.equals("ARIZONA"))
        {
            UTC = 6;
        }
        else if(Arrays.asList(pacific_Time).contains(state))
        {
            if(isDST_US)
            {
                UTC = 6;
            }
            else
            {
                UTC = 5;
            }
        }
        else if(state.equals("ALASKA"))
        {
            String options[] = {"Aleutian", "Rest of Alaska"};
            String choice = (String) JOptionPane.showInputDialog(ZClock_JFrame, "Which part of Alaska?",
                    "Question", JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

            if(choice != null) {

                if (choice.equals("Aleutian")) {
                    UTC = 2;
                }
                else
                    {
                    if (isDST_US) {
                        UTC = 5;
                    } else {
                        UTC = 4;
                    }
                }
            }
        }
        else if(state.equals("HAWAII"))
        {
            UTC = 2;
        }

        if(UTC != -1)
        {
            searchedStaPro = state;
        }

        return UTC;
    }


    /*    ***CONVERT TO TIME FUNCTIONS***   */
    private boolean isPostiveUTC(int UTC)
    {
        if((UTC + 14) < 15)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    public String conAndDisplay12()
    {
        // Declare local variables
        int userUTC = getUserUTC();
        int searchedUTC = getSearchedCountry_UTC();
        Calendar currentTime = getSysTime();
        String ampm;
        int hour = currentTime.get(Calendar.HOUR_OF_DAY);
        int minutes = currentTime.get(Calendar.MINUTE);
        int month = currentTime.get(Calendar.MONTH);
        int day = currentTime.get(Calendar.DAY_OF_MONTH);
        int year = currentTime.get(Calendar.YEAR);

        // Converting variables
        int differenceBetween = 0;
        boolean bUserUTC_Pos = isPostiveUTC(userUTC);
        boolean bSearchedUTC_Pos = isPostiveUTC(searchedUTC);

        if(userUTC > searchedUTC)
        {
            if(bUserUTC_Pos && bSearchedUTC_Pos)
            {
                differenceBetween = userUTC - searchedUTC;
            }
            else if((!bUserUTC_Pos) && (!bSearchedUTC_Pos))
            {
                differenceBetween = ((searchedUTC + (userUTC * -1)));
            }
            else
            {
                differenceBetween = (userUTC + (searchedUTC * -1)) * -1;
            }
        }
        else if(userUTC < searchedUTC)
        {
            if(bUserUTC_Pos && bSearchedUTC_Pos)
            {
                differenceBetween = searchedUTC - userUTC;
            }
            else if((!bUserUTC_Pos) && (!bSearchedUTC_Pos))
            {
                differenceBetween = (userUTC + (searchedUTC * -1)) * -1;
            }
            else
            {
                differenceBetween = (searchedUTC +(userUTC * -1));
            }
        }

        // convert time difference
        if(differenceBetween != 0)
        {
            hour += (differenceBetween / 100);
            minutes += (differenceBetween % 100);

            // Check if minutes are over 60
            if(minutes > 59)
            {
                hour += minutes / 60;
                minutes = minutes % 60;
            }
        }

        // Format the time
        if(hour > 24)
        {
            day++;
            hour -= 24;
            int amountOfDayAhead = (hour  / 24);

            day -= (amountOfDayAhead * 24);
            day += amountOfDayAhead;
        }
        if(hour > 12)
        {
            ampm = "PM";
            hour -= 12;
        }
        else
        {
            ampm = "AM";
        }
        String minutes_st = Integer.toString(minutes);
        if(minutes < 10)
        {
            minutes_st = "0" + minutes_st;
        }

        return hour + ":" + minutes_st + ampm + " " + month + "/" + day + "/" + year;

    }
    public String conAndDisplay24()
    {
        // Declare local variables
        int userUTC = getUserUTC();
        int searchedUTC = getSearchedCountry_UTC();
        Calendar currentTime = getSysTime();
        int hour = currentTime.get(Calendar.HOUR_OF_DAY);
        int minutes = currentTime.get(Calendar.MINUTE);
        int month = currentTime.get(Calendar.MONTH);
        int day = currentTime.get(Calendar.DAY_OF_MONTH);
        int year = currentTime.get(Calendar.YEAR);

        // Converting variables
        int differenceBetween = 0;
        boolean bUserUTC_Pos = isPostiveUTC(userUTC);
        boolean bSearchedUTC_Pos = isPostiveUTC(searchedUTC);

        if(userUTC > searchedUTC)
        {
            if(bUserUTC_Pos && bSearchedUTC_Pos)
            {
                differenceBetween = userUTC - searchedUTC;
            }
            else if((!bUserUTC_Pos) && (!bSearchedUTC_Pos))
            {
                differenceBetween = ((searchedUTC + (userUTC * -1)));
            }
            else
            {
                differenceBetween = (userUTC + (searchedUTC * -1)) * -1;
            }
        }
        else if(userUTC < searchedUTC)
        {
            if(bUserUTC_Pos && bSearchedUTC_Pos)
            {
                differenceBetween = searchedUTC - userUTC;
            }
            else if((!bUserUTC_Pos) && (!bSearchedUTC_Pos))
            {
                differenceBetween = (userUTC + (searchedUTC * -1)) * -1;
            }
            else
            {
                differenceBetween = (searchedUTC +(userUTC * -1));
            }
        }


        // convert time difference
        if(differenceBetween != 0)
        {
            hour += (differenceBetween / 100);
            minutes += (differenceBetween % 100);

            // Check if minutes are over 60
            if(minutes > 59)
            {
                hour += minutes / 60;
                minutes = minutes % 60;
            }
        }

        // Format time
        if(hour > 24)
        {
            day++;
            hour -= 24;
            int amountof24Ahead = (hour / 24);

            hour -= (24 * amountof24Ahead);
            day += amountof24Ahead;
        }
        String hour_st = Integer.toString(hour);
        if(hour < 10)
        {
            hour_st = "0" + hour_st;
        }
        String minutes_st = Integer.toString(minutes);
        if(minutes < 10)
        {
            minutes_st = "0" + minutes_st;
        }

        return hour_st + ":" + minutes_st + " " + month + "/" + day + "/" + year;

    }



}
